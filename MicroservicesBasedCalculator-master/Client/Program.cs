﻿using Grpc.Core;
using System;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            Console.WriteLine("Introduceti data nasterii:");

            do
            {

                var date = Console.ReadLine();
                if (date.Length != 10)
                {
                    Console.WriteLine("\nAti introdus o data gresita.");
                    break;
                }

                int luna;
                bool success = Int32.TryParse(date.Substring(0,2), out luna);
                if (success)
                {
                    if(luna<1 || luna >12)
                    {
                        Console.WriteLine("\nAti introdus o data gresita.");
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("\nAti introdus o data gresita.");
                    break;
                }

                if (date[2] != '/' || date[5] != '/')
                {
                    Console.WriteLine("\nAti introdus o data gresita.");
                    break;
                }

                int an;
                success = Int32.TryParse(date.Substring(6, 4), out an);
                if (success)
                {
                    if (an<0)
                    {
                        Console.WriteLine("\nAti introdus o data gresita.");
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("\nAti introdus o data gresita.");
                    break;
                }

                int zi;
                success = Int32.TryParse(date.Substring(3, 2), out zi);
                if (success)
                {
                    if(zi<1 || zi>31)
                    {
                        Console.WriteLine("\nAti introdus o data gresita.");
                        break;
                    }

                    if (an%4==0 && luna==2 && zi>29)
                    {
                        Console.WriteLine("\nAti introdus o data gresita.");
                        break;
                    }

                    if (an % 4 != 0 && luna == 2 && zi > 28)
                    {
                        Console.WriteLine("\nAti introdus o data gresita.");
                        break;
                    }

                    if((luna==4 || luna==6 || luna==9 || luna==11) && zi>30)
                    {
                        Console.WriteLine("\nAti introdus o data gresita.");
                        break;
                    }

                }
                else
                {
                    Console.WriteLine("\nAti introdus o data gresita.");
                    break;
                }

                var client = new Generated.ZodiacOperation.ZodiacOperationClient(channel);
                var response = client.FindZodiac(new Generated.ZodiacRequest
                {
                    Month = luna,
                    Day = zi,
                    Year = an,
                });

                Console.WriteLine("\nClient received response: {0}", response.Zodiac);
            } while (true);


            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
