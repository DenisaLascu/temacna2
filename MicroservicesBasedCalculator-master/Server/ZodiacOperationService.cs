﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Server
{
    internal class ZodiacOperationService : Generated.ZodiacOperation.ZodiacOperationBase
    {
        public override Task<ZodiacResponse> FindZodiac(ZodiacRequest request, ServerCallContext context)
        {
            var result = "";
            List<string> zodii = new List<string>();
            zodii.Add("Varsator");
            zodii.Add("Pesti");
            zodii.Add("Berbec");
            zodii.Add("Taur");
            zodii.Add("Gemeni");
            zodii.Add("Rac");
            zodii.Add("Leu");
            zodii.Add("Fecioara");
            zodii.Add("Balanta"); 
            zodii.Add("Scorpion");
            zodii.Add("Sagetator");
            zodii.Add("Capricorn");
            List<string> intervale = new List<string>();
            var lines = File.ReadLines("zodii.txt");
            foreach (var line in lines)
                intervale.Add(line);
            Int32 zodiacDay = Int32.Parse(intervale[request.Month - 1].Substring(3, 2));
            if ( zodiacDay<= request.Day)
                result = zodii[request.Month-1];
            else
                if (request.Month == 1)
                result = zodii[11];
            else
                result = zodii[request.Month - 2];
            return Task.FromResult(new ZodiacResponse() { Zodiac = result });
        }
    }
}
